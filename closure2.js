function counter(initValue) {
    var currentValue = initValue;
    var logCurrentValue = function() {
        console.log('currentValue = ' + currentValue);
    }
    var increment = function(step) {
        currentValue += step;
        logCurrentValue();
    };
    var decrement = function(step) {
        currentValue -= step;
        logCurrentValue();
    }
    return {increment: increment,
            decrement: decrement};
    }           
            var myCounter1 = counter(0);
            var myCounter2 = counter(3);

            myCounter1.increment(2);
            myCounter1.increment(2);
            myCounter1.decrement(1);
            myCounter2.decrement(1);
        
    
    

