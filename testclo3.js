const limitFunctionCallCount = require('./limitFunctionCallCount');
let counter = limitFunctionCallCount(() => {console.log('callback invoked')}, 2);
counter.invoke();
counter.invoke();
counter.invoke();
counter.invoke();
