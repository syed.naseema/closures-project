function limitFunctionCallCount(cb,n) {
    let count = n;
    return {
        invoke: function() {
            if(count-- > 0) {
                cb(count);
            }else{
                null;
            }
        }
    }
}
