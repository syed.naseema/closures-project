const cacheFunction = require("./cacheFunction.js");
let cb = function(...a) {
    let sum = 0;
    for(let i=0;i<a.length;i++){
        sum+=a[i];
    }
    console.log(sum);
}
cacheFunction(cb)(4,5,6);
cacheFunction(cb)(7,8);
cacheFunction(cb)(9);
cacheFunction(cb)(2,5,6);
cacheFunction(cb)(4,2);
cacheFunction(cb)(1,2,3);
