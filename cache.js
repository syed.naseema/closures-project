let cache = {};
function cacheFunction(cb) {
  return ((...arguments)=>{
     let a = 0;
     for(let i=0; i< arguments.length; i++){
        if(cache[arguments[i]] !=="seen"){
           cache[arguments[i]] = "seen";
           a = 1;
        }
     }  
     //console.log(cache);
     if(a===1){
        cb(...arguments);
     }
     else {
          console.log(cache);
     }
});


}
module.exports = cacheFunction;
